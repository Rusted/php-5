<html>

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>

<body>
    <p>
        <?php
// var_dump($_POST);
$salaries = explode(',', $_POST['salaries']);
// var_dump($salaries);

$salaryGroups = [
    'iki (įskaitant) 380' => 0,
    'nuo 380 iki (įskaitant) 820' => 0,
    'nuo 830 iki (įskaitant) 1500' => 0,
    'virš 1500' => 0,
];

foreach ($salaries as $salary) {
    if ($salary <= 380) {
        $salaryGroups['iki (įskaitant) 380']++;
    } elseif ($salary <= 820) {
        $salaryGroups['nuo 380 iki (įskaitant) 820']++;
    } elseif ($salary <= 1500) {
        $salaryGroups['nuo 830 iki (įskaitant) 1500']++;
    } else {
        $salaryGroups['virš 1500']++;
    }
}

?>
    </p>
    <form method="POST">
        <input name="salaries" type="text" />
        <input type="submit" value="Siųsti" />
    </form>

    <table border="1">
        <tr>
            <th>Atlyginmo grupė</th>
            <th>Darbuotojų skaičius</th>
        </tr>
        <?php foreach ($salaryGroups as $groupLabel => $countOfWorkers) {?>
        <tr>
            <td>
                <?php echo $groupLabel; ?>
            </td>
            <td>
                <?php echo $countOfWorkers ?>
            </td>
        </tr>
        <?php }?>
    </table>
    <canvas id="myChart"></canvas>
    <script src="workerGroupsChart.js"></script>
    <script>
        var data = [<?php echo implode(',', $salaryGroups); ?>];
        var labels = ['<?php echo implode("', '", array_keys($salaryGroups)); ?>'];
        initWorkerGroupsChart("myChart", labels, data);
    </script>
</body>

</html>